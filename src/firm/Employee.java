package firm;

public class Employee implements Info, ChangeSalary {
	
	private String name;
	private String lastName;
	private double salary;
	private String title;
	
	public Employee(String name, String lastName, double salary, String title){
		this.name = name;
		this.lastName= lastName;
		this.salary= salary;
		this.title= title;
	}
	
	public String getName() {
		return name;
	}


	public String getLastName() {
		return lastName;
	}


	public double getSalary() {
		return salary;
	}
	
	public String getTitle() {
		return title;
	}

	
	@Override
	public void showInfo() {
		System.out.println ("name=" + name + ",lastname=" + lastName
				+ ",salary=" + salary + ",title=" + title );
		
	}


	@Override
	public void raiseSalary(double byPercent) throws NotNegativePercentException {
		
		if (byPercent < 0) {
			throw new NotNegativePercentException();
		}
		
		double r = salary * byPercent / 100;
		salary += r;
		
	}


	@Override
	public void lowerSalary(double byPercent) throws NotNegativePercentException, NotNegativeSalaryException {
		
		if (byPercent < 0) {
			throw new NotNegativePercentException();
		}
		
		double s = salary;
		double r = s * byPercent / 100;
		s -= r;
		
		if (s < 0) {
			throw new NotNegativeSalaryException();
		}
		
		r = salary * byPercent / 100;
		salary -= r;
		
	}
	
	public void extra() {
		System.out.println(name + ", drinks and fruits in the corporate kitchen are at your disposal.");
	}
	

}
