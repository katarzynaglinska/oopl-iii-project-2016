package firm;

public class NotNegativePercentException extends Exception {

	@Override
	public String getMessage() {
		return "WARNING: the percentage should be strictly positive.";
	}

}