package firm;

public class CleaningLady extends Employee {

	public CleaningLady(String name, String lastName, double salary) {
		super(name, lastName, salary, "Cleaning Lady");
	}
	
	@Override
	public void extra() {
		System.out.println( getName() + ", You get a ticket to the spa. ");
		
	}

}
