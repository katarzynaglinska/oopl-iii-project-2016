package firm;

public interface ChangeSalary {
	
	public void raiseSalary(double byPercent) throws NotNegativePercentException;
	public void lowerSalary(double byPercent) throws NotNegativePercentException, NotNegativeSalaryException ;

}
