package firm;

public class NotNegativeSalaryException extends Exception {

	@Override
	public String getMessage() {
		return "WARNING: the salary should be strictly positive.";
	}

}