package firm;

public class Boss extends Employee
{
	private double bonus;
	
	public Boss(String name, String lastName, double salary){
		super(name, lastName, salary, "Boss");
		bonus = 0;
	}
	
	
	public double getSalary(){
		double firstSalary = super.getSalary();
		return firstSalary + bonus;
	}

	
	public void setBonus(double bonus){
		this.bonus = bonus;
	}
	
	@Override
	public void extra() {
		System.out.println( getName() + ", I'm sending you a paid vacation! ");
		
	}	
	
}
