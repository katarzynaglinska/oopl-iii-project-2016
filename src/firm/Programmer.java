package firm;

public class Programmer extends Employee {

	public Programmer(String name, String lastName, double salary) {
		super(name, lastName, salary, "Programmer");
	}
	
	@Override
	public void extra() {
		System.out.println( getName() + ", You get a company laptop and a pass to the gym. ");
		
	}

}
