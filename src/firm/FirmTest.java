package firm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirmTest 
{
	public static void main(String[] args)
	{
		Employee em = new Employee ("Zofia", "Markowska", 3000, "Sekretarka" );
		Boss boss = new Boss("Kacper", "Koz�owski", 10000);
		Programmer programmer = new Programmer("Anastazja", "Kowalska", 6000);
		CleaningLady lady = new CleaningLady("Alina", "Majewska", 2500);
		TeamLeader leader = new TeamLeader ("Krzysztof", "Warakomski", 7000);
		
		
		try {
		lady.raiseSalary(-20);
		} catch (NotNegativePercentException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		System.out.println(lady.getSalary());
		lady.showInfo();
		
		try {
		lady.lowerSalary(120);
		} catch (NotNegativePercentException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
		catch (NotNegativeSalaryException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		System.out.println(lady.getSalary());
		lady.showInfo();
		
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		employees.add(em);
		employees.add(boss);
		employees.add(programmer);
		employees.add(lady);
		employees.add(leader);
		
		System.out.println("My Emploees :");
		for(Employee e : employees){
			e.showInfo();
			e.extra();
		}
		
		System.out.println("My Emploees with their PIN numbers:");
		Map <String, Employee> staff = new HashMap<String, Employee>();
		staff.put("85982399845", em);
		staff.put("56231255634", boss);
		staff.put("94859833044", programmer);
		staff.put("74562238984", lady);
		staff.put("84653111984", leader);
		
		for(Map.Entry<String, Employee> entry : staff.entrySet()){
			String key= entry.getKey();
			Employee value = entry.getValue();
			System.out.println("PIN number="+key + ", value=" + value);
		}
		
	}

}
